package sbu.cs.excercises;

import java.util.List;

public class ExerciseLecture6 {

    /**
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     * @param array an integer array
     */
    public long calculateEvenSum(int[] array) {
        return 0L;
    }

    /**
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     * @param array an integer array
     */
    public int[] reverseArray(int[] array) {
        return null;
    }

    /**
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     * @param matrix1 the first matrix
     * @param matrix2 the second matrix
     */
    public double[][] matrixProduct(double[][] matrix1, double[][] matrix2) throws RuntimeException {
        return null;
    }

    /**
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     * @param names a two-dimensional array of strings
     */
    public List<List<String>> arrayToList(String[][] names) {
        return null;
    }

    /**
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     * @param n the number n
     */
    public List<Integer> primeFactors(int n) {
        return null;
    }

    /**
     *   implement a function that return a list of words in a given string.
     *   consider that words are separated by spaces, commas,questions marks....
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        return null;
    }
}
